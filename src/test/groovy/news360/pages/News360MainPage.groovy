package news360.pages

import geb.Page
import news360.modules.News360RestorePasswordModule
import news360.modules.News360SignInModule
import news360.modules.News360SignUpModule

class News360MainPage extends Page {
    static url = "http://news360.com"

    static content ={
        def sout = new StringBuilder()
        def serr = new StringBuilder()
        def proc = 'ls /badDir'.execute()
        proc.consumeProcessOutput(sout, serr)
        
        startReadingButton {$("a",text:"Start reading")}
        showSignInFormButton { $("a", 1,text: "Sign in with email") }
        signInForm {module News360SignInModule}
        signUpForm {module News360SignUpModule}
        restoreForm {module News360RestorePasswordModule}
        closePopUp {$("div.close")}
    }

    static at = {
        title == "News360: Your personalized news reader app"
    }

    def showPopUp(){
        startReadingButton.click()
        showSignInFormButton.click()
    }

    def closePopUp(){
        closePopUp.click()
    }
}