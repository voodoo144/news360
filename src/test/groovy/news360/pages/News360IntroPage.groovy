package news360.pages

import geb.Page

/**
 * Created by user on 16.01.2017.
 */
class News360IntroPage extends Page {
    static url = "https://news360.com/web/intro/"
    static at={
         waitFor{$("h2",0)}.text() == "Welcome to News360"
    }
}
