package news360.pages

import geb.Page

/**
 * Created by user on 16.01.2017.
 */
class News360PersonalizedPage extends Page {
    static url = "https://news360.com/web/setup/categories/"
    static at = {
        waitFor{$("p",text:"Looks pretty empty, huh?")}.displayed
    }
}
