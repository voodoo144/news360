package news360.modules

import geb.Module

/**
 * Created by user on 15.01.2017.
 */
class News360RestorePasswordModule extends Module {
    static base = { $("div.simpleauth") }
    static content = {
        emailInput { $("input.email", 3) }
        confirmButton { $("button.reset-button",1) }
        errorHolder {$("div.error-message",0)}
    }

    def fillForm(email){
        emailInput << email
    }
}
