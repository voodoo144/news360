package news360.modules

import geb.Module

/**
 * Created by user on 15.01.2017.
 */
class News360SignInModule extends Module {
    static base = { $("div.simpleauth") }

    static content = {
        switchButton { $("a.signup",3) }
        emailInput { $("input.email",5) }
        passwordInput { $("input.password", 5) }
        confirmButton { $("button.signin-button",1) }
        cancelButton { $("button.cancel-button",3) }
        forgotPasswordButton {$("a.forgotpass",1)}
        errorHolder {$("ul.parsley-error-list")}
        loginError {$("div.error-message",1)}
    }

    def fillWith(username, password) {
        emailInput << username
        passwordInput << password
    }

    def submit(){
        confirmButton.click()
        sleep(2000)
    }

    def switchToSignUp(){
        switchButton.click()
    }
}
