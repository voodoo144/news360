package news360.modules

import geb.Module

/**
 * Created by user on 15.01.2017.
 */
class News360SignUpModule extends Module {
    static base = { $("div.simpleauth") }
    static content = {
        switchButton { $("a.login", 3) }
        emailInput { $("input.email", 4) }
        passwordInput { $("input", id: "popuppassword") }
        passwordConfirmInput { $("input.confirmpassword", 3,type: "password") }
        confirmButton { $("button.signup-button",1) }
        cancelButton { $("button.cancel-button",2) }
        errorHolder {$("ul.parsley-error-list")}
    }

    def fillWith(username, password, confirmedPassword) {
        emailInput << username
        passwordInput << password
        passwordConfirmInput << confirmedPassword
    }

    def submit(){
        confirmButton.click()
    }
}