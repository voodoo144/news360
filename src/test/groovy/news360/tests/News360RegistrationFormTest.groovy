package news360.tests

import news360.pages.News360IntroPage
import news360.pages.News360MainPage
import news360.pages.News360PersonalizedPage
import news360.utils.CustomTest
import news360.utils.Faker
import org.junit.Test
import org.junit.runner.RunWith
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

@RunWith(DataProviderRunner.class)
class News360RegistrationFormTest extends CustomTest {
    //Already registered user for test purposes
    static String realEmail = "numquam.alias.33@example.com"
    static String realPassword = "123qwe"

    @DataProvider
    public static Object[][] signUpDataProvider() {
        return [
                [Faker.email(), "123qwe", "12345", true],
                [Faker.email(), "", "", true],
                ["", "", "", true],
                [Faker.email(), "123qwe", "123qwe", false],
                [Faker.email(), "123qwe", "", true],
                [Faker.email(), "", "12455", true],
                [Faker.word(), "123qwe", "123qwe", true],
                [Faker.word() + "@", "123qwe", "123qwe", true],
                ["@" + Faker.word(), "123qwe", "123qwe", true],
                ["@example.com", "123qwe", "123qwe", true],
        ]
    }

    @DataProvider
    public static Object[][] signInDataProvider() {
        return [
                [realEmail, realPassword, false],
                [Faker.email(), Faker.word(), true],
                [Faker.email(), "", true],
                ["", "", true],
                ["", Faker.word(), true],
                [realEmail, realPassword + " ", true],
                ["  " + realEmail + "  ", realPassword, false],
        ]
    }

    @DataProvider
    public static Object[][] restorePasswordDataProvider() {
        return [[Faker.email(), false], [Faker.word(), true], Faker.word() + "@", true]
    }


    @Test
    @UseDataProvider("signUpDataProvider")
    void registrationWithEmailPasswordProviderTest(username, password, confirmPassword, withError) {
        to News360MainPage
        showPopUp()
        signInForm.switchToSignUp()
        signUpForm.fillWith(username, password, confirmPassword)
        signUpForm.submit()
        if (withError)
            assert waitFor { signUpForm.errorHolder }.size() > 0
        else {
            sleep(3000)
            at News360IntroPage
        }
    }

    @Test
    @UseDataProvider("signInDataProvider")
    void signInWithEmailAndPasswordTest(username, password, withError) {
        to News360MainPage
        showPopUp()
        signInForm.fillWith(username, password)
        signInForm.submit()
        if (withError) {
            if (username == "" || password == "")
                assert waitFor { signInForm.errorHolder }.size() > 0
            else {
                assert $("div.error-message", 1).text() == "Invalid login or password"
            }
        } else {
            sleep(3000)
            at News360PersonalizedPage
        }
    }

    @Test
    @UseDataProvider("restorePasswordDataProvider")
    void restorePasswordTest(email, withError) {
        to News360MainPage
        showPopUp()
        signInForm.forgotPasswordButton.click()
        restoreForm.fillForm(email)
        restoreForm.confirmButton.click()
        sleep(1000)
        if (withError)
            assert restoreForm.errorHolder.text() == "Email isn't found. Please try again"
        else {
            assert $("div.success-message",1).text() == "We emailed you a link to reset your password"
        }
    }

    @Test
    void openCloseFormTest() {
        to News360MainPage
        showPopUp()
        closePopUp()
        assert !waitFor { $("div.simplepopup") }.displayed
    }

    @Test
    void cancelTest() {
        to News360MainPage
        showPopUp()
        signInForm.cancelButton.click()

        showPopUp()
        signInForm.switchToSignUp()
        signUpForm.cancelButton.click()

    }


}