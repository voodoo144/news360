import org.openqa.selenium.firefox.FirefoxDriver

waiting {
    timeout = 3
}

cacheDriver=false

environments {
    firefox {
        driver = { new FirefoxDriver() }
        driver
    }
}
