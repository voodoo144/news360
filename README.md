### News360 Test task ###

* This repo contains several UI test cases for http://news360.com
* Version 1.0

### How to run? ###

* On your machine should be installed JDK at least 1.8_45
* Should be defined JAVA_HOME env variable
* JAVA_HOME should point to installed JDK folder
* Should be installed FIrefox browser latest version(48.0 at this moment)

To run test - you need to clone this repo and run command:
`./gradlew test` - in case of Linux/MacOs/Unix
or
`./gradlew.bat test` - in case of Windows